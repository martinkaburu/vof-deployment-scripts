#!/bin/bash
pg_dump -F p -f /vof/backups/vof-production-db-backup-`date +"%Y"`-`date +"%m"`-`date +"%d"`.sql
echo "Database backup was created:"
# prune old backups
find /vof/backups -maxdepth 1 -mtime +14 -name "*.sql" -exec rm -rf '{}' ';'
