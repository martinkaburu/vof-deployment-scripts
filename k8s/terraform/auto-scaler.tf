resource "kubernetes_horizontal_pod_autoscaler" "vof_tracker_auto" {
 depends_on = ["kubernetes_deployment.vof_tracker_deployment"]
 metadata {
   name      = "${var.deployment_name}-hpa"
   namespace = "${kubernetes_namespace.vof_tracker_namespace.id}"
 }
 spec {
   max_replicas = 4
   min_replicas = 2
   target_cpu_utilization_percentage = "80"
   scale_target_ref {
     api_version = "extensions/v1beta1"
     kind = "Deployment"
     name = "${var.deployment_name}"
   }
 }
}
