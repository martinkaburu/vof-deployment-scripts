variable "deployment_name" {
  type = "string"
}

variable "vof_tracker_image" {
  type = "string"
}

variable "deployment_port" {
  type = "string"
}

variable "namespace" {
  type = "string"
}

variable "region" {
  type = "string"
}

variable "project" {
  type = "string"
}

variable "vof_domain_name" {
  type = "string"
}

variable "ingress_template_file" {
  type = "string"
}

variable "db_instance_tier" {
  type    = "string"
  default = "db-f1-micro"
}

variable "db_backup_start_time" {
  type    = "string"
  default = "00:00"
}

variable "vof_database_instance" {
  type = "string"
}
