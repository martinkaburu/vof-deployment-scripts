resource "kubernetes_persistent_volume_claim" "redis_data" {
  depends_on = ["kubernetes_storage_class.redis_storage"]
  metadata {
    name                   = "redis-data-${kubernetes_namespace.vof_tracker_namespace.id}"
    namespace              = "${kubernetes_namespace.vof_tracker_namespace.id}"
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    storage_class_name = "redis-storage-class"
    resources {
      requests = {
        storage            = "10Gi"
      }
    }
  }
}

resource "kubernetes_storage_class" "redis_storage" {
  metadata {
    name = "redis-storage-class"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  reclaim_policy      = "Retain"
  parameters = {
    type = "pd-ssd"
  }
}
