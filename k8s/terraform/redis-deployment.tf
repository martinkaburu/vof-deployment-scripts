resource "kubernetes_deployment" "redis_deployment" {
  depends_on = ["kubernetes_persistent_volume_claim.redis_data"]

  metadata {
    name      = "redis"
    namespace = "${kubernetes_namespace.vof_tracker_namespace.id}"

    labels {
      app     = "redis"
      version = "3.2.5"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels {
        app = "redis"
      }
    }

    template {
      metadata {
        namespace = "${kubernetes_namespace.vof_tracker_namespace.id}"

        labels {
          app     = "redis"
          version = "3.2.5"
        }
      }

      spec {
        container {
          image = "redis:3.2.5"
          name  = "redis"

          port {
            container_port = "6379"
            name           = "redis"
          }

          volume_mount {
            mount_path = "/data"
            name       = "redis-data-${kubernetes_namespace.vof_tracker_namespace.id}"
          }

          readiness_probe {
            tcp_socket {
              port = 6379
            }

            timeout_seconds       = 30
            period_seconds        = 10
            initial_delay_seconds = 60
          }

          image_pull_policy = "IfNotPresent"
        }

        volume {
          name = "redis-data-${kubernetes_namespace.vof_tracker_namespace.id}"

          persistent_volume_claim {
            claim_name = "redis-data-${kubernetes_namespace.vof_tracker_namespace.id}"
          }
        }
      }
    }
  }
}
