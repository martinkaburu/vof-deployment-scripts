resource "kubernetes_deployment" "vof_tracker_deployment" {
  depends_on = ["google_sql_database_instance.vof-database-instance", "kubernetes_config_map.vof_config_map"]

  metadata {
    name      = "${var.deployment_name}"
    namespace = "${kubernetes_namespace.vof_tracker_namespace.id}"

    labels {
      app = "${var.deployment_name}"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels {
        app = "${var.deployment_name}"
      }
    }

    template {
      metadata {
        namespace = "${kubernetes_namespace.vof_tracker_namespace.id}"

        labels {
          app = "${var.deployment_name}"
        }
      }

      spec {
        container {
          image = "${var.vof_tracker_image}"
          name  = "${var.deployment_name}"

          port {
            container_port = 3000
            name           = "http"
          }

          env {
            name  = "DB_HOST"
            value = "localhost"
          }

          env {
            name  = "DB_USER"
            value = "${random_id.vof-db-user.b64}"
          }

          env {
            name  = "PGPASS"
            value = "${random_id.vof-db-user-password.b64}"
          }

          env {
            name  = "DB_NAME"
            value = "k8s-${var.namespace}-vof-database"
          }

          image_pull_policy = "Always"

          resources {
            requests {
              cpu    = "0.5"
              memory = "1Gi"
            }

            limits {
              cpu    = "1"
              memory = "3Gi"
            }
          }

          readiness_probe {
            tcp_socket {
              port = 80
            }

            timeout_seconds       = 30
            period_seconds        = 30
            initial_delay_seconds = 30
          }
        }

        container {
          image = "gcr.io/cloudsql-docker/gce-proxy:1.09"
          name  = "cloudsql-proxy"

          env {
            name  = "DB_INSTANCE_NAME"
            value = "${var.vof_database_instance}"
          }

          volume_mount {
            mount_path = "/cloudsql/secrets"
            name       = "cloudsql-credentials-${kubernetes_namespace.vof_tracker_namespace.id}"
          }

          command = ["/cloud_sql_proxy", "-instances=${var.vof_database_instance}=tcp:5432", "-credential_file=/cloudsql/secrets/credentials.json"]
        }

        volume {
          name = "cloudsql-credentials-${kubernetes_namespace.vof_tracker_namespace.id}"

          config_map {
            name = "cloudsql-credentials-${kubernetes_namespace.vof_tracker_namespace.id}"
          }
        }
      }
    }
  }
}

resource "kubernetes_config_map" "vof_config_map" {
  metadata {
    name      = "cloudsql-credentials-${kubernetes_namespace.vof_tracker_namespace.id}"
    namespace = "${kubernetes_namespace.vof_tracker_namespace.id}"
  }

  data = {
    "credentials.json" = "${file("secrets/google-service-key.json")}"
  }
}
