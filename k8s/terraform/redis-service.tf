resource "kubernetes_service" "redis_service" {
  metadata {
    name            = "redis-${kubernetes_namespace.vof_tracker_namespace.id}"
    namespace       = "${kubernetes_namespace.vof_tracker_namespace.id}"
  }

  spec {
    selector {
      app           = "redis"
    }
    port {
      port          = 6379
      target_port   = 6379
      name          = "redis"
    }
    type            =   "NodePort"
  }
}
