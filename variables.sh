#! /bin/bash

# create array of variables in template files
create_variables (){
    VARIABLES+=(
    project
    region
    zone
    bucket
    prefix
    environment
    credentials
    namespace
    deployment_name
    deployment_port
    vof_tracker_image
    vof_domain_name
    vof_domain_name
    machine_type
    vof_database_instance
    )
}
create_variables
